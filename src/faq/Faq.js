import './Faq.css'

function Faq() {
  const data = [
    {
      title: '1. What is Stickman Investor?',
      content: 'Stickman Investor is collection of 500 investment lovers, who have decided to raise their capital in order to carry out certain financial operations.'
    },
    {
      title: '2. How does Stickman Investor work?',
      content: 'The money collected by selling these 500 Stickman Investors will be used to do cake staking, which will make a weekly profit.'
    },
    {
      title: '3. What will be done with the weekly profit?',
      content: 'As appreciation to our Stickman Investors for buying our collection, we will give this affection back by making weekly draw.'
    },
    {
      title: '4. How will the draw take place?',
      content: 'Every Sunday, a Discord live will be carried out, in which, a random winners program will give NFT address. As soon as we have this address and after checking who is the owner of the address, the prize shall be given to the winner.'
    },
    {
      title: '5. How many chances of winning a draw do I have?',
      content: 'Once a week, you, the holders will vote how the prize should be given. For example, if one week there is a prize of 600 dollars, you will vote whether we should give a unique 600 dollars prize or 3 200 dollars prizes.'
    },
    {
      title: '6. What will happen if cake doubles its value?',
      content: 'In that case, a voting will decide whether we distribute all that went up or whether we leave it in order to have more weekly budget.'
    },
    {
      title: '7. Can I sell my NFT?',
      content: 'We are going to try getting in touch with different marketplaces who work with Solana to sell our collection there. If it was no possible, we would tried to create our own marketplace.'
    },
    {
      title: '8. Which other advantages will I have if I buy this NFT?',
      content: 'First of all, you could say that YOU ARE A STICKMAN INVESTOR!!!! Also, you will have some privileges in coming projects.'
    }
  ]

  return (<section id="faq" >
    <h3 className="title">FAQ</h3>
    {data.map((data, index) => <div className="content" key={index}>
      <div className="title-content">{data.title}</div>
      <p>{data.content}</p>
    </div>)}
  </section>)
}

export default Faq;