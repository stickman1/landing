import CEOImage from "./../assets/StickmaninvestorCEO.svg";
import AdminImage from "./../assets/StickmaninvestorAdmin.svg";
import DeveloperImage from "./../assets/StickmaninvestorDeveloper.svg";
import ArtistImage from "./../assets/StickmaninvestorArtist.svg";
import AdvisorImage from "./../assets/StickmaninvestorAdvisor.svg";
import logoNegativo2 from "./../assets/logo negativo 2.svg";
import discord from "../assets/discord.svg";
import twitter from "../assets/twitter.svg";
import "./Team.css";

function Team() {
  const images = new Map([
    ["CEO", CEOImage],
    ["Admin", AdminImage],
    ["Web Developer", DeveloperImage],
    ["Artist", ArtistImage],
    ["Advisor", AdvisorImage],
  ]);
  


  const teamates = [
    {
      name: "Alejandro",
      lastName: "Balan",
      position: "CEO",
      // twitter: "twitter.com",
      discord: "KingMate#7340"
    },
    /*{
      name: "Teammate1",
      lastName: "STICKLASTNAME",
      position: "Admin",
      twitter: "twitter.com",
      discord: "discord.com",
    },*/
    {
      name: "César",
      lastName: "Calle",
      position: "Web Developer",
      twitter: "@cace_tres_",
      discord: "3tonycesar#7921",
    },
    {
      name: "Ignacio",
      lastName: "Lema Lewis",
      position: "Artist",
      twitter: "@Lemushki",
      discord: "Lemushki#5088",
    },
    {
      name: "Fede",
      lastName: "Argemí",
      position: "Advisor",
      // twitter: "twitter.com",
      discord: "Fede Argemi #4311",
    },
    /*{
      name: "Teammate1",
      lastName: "STICKLASTNAME",
      position: "Advisor",
      twitter: "twitter.com",
      discord: "discord.com",
    },*/
  ];

  return (
    <section id="team">
      <h3 className="title">Team</h3>
      <div className="container">
        {teamates.map((teamate, index) => (
          <div className="teamate" key={index}>
            <div className="header">
              <img
                src={
                  images.has(teamate.position)
                    ? images.get(teamate.position)
                    : logoNegativo2
                }
                alt={teamate.position}
              />
            </div>
            <div className="body">
              <p className="name">{teamate.name}</p>
              <p className="lastname">{teamate.lastName}</p>
              <p className="position">{teamate.position}</p>
              {teamate.twitter ? (
                <div>
                  <a href={"https://twitter.com/"+teamate.twitter.replace("@",'')} target="_blank" rel="noreferrer">
                    <img src={twitter} alt="twitter" />
                    {teamate.twitter}
                  </a>
                </div>
              ) : (
                <></>
              )}
              {teamate.discord ? (
                <div>
                  <span className="social-item">
                    <img src={discord} alt="discord" /> {teamate.discord}
                  </span>
                </div>
              ) : (
                <></>
              )}
            </div>
          </div>
        ))}
      </div>
    </section>
  );
}

export default Team;
