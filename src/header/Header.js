import "./Header.css";
import React from "react";
import logo from "../assets/logo estrecho.svg";
import discord from "../assets/discord.svg";
import twitter from "../assets/twitter.svg";
import instagram from "../assets/instagram.svg";
import legendary from "../assets/Stickmaninvestor-legendary.svg";
import * as util from "../util";

function Header({ goTo, routeActive }) {
  const menus = [
    { title: "Welcome", path: "welcome" },
    { title: "Road Map", path: "roadmap" },
    { title: "Team", path: "team" },
    { title: "FAQ", path: "faq" },
  ];
  const middle = Math.floor(menus.length / 2);
  return (
    <section id="header">
      <header className="menu navigator">
        {menus.slice(0, middle).map((menu, index) => (
          <div className="item-container" key={"i-"+index}>
            <a
              key={index}
              href={"#" + menu.path}
              className={menu.path === routeActive ? "item" : "item"}
              
            >
              {menu.title}
            </a>
          </div>
        ))}
        <div
          className="logo-container"
          onClick={(e) => {
            goTo("header", e);
          }}
        >
          <img src={logo} className="app-logo" alt="STICKMAN INVESTOR" />
        </div>
        {menus.slice(middle, menus.length).map((menu, index) => (
          <div className="item-container"  key={"j-"+index}>
            <a
              key={index}
              href={"#" + menu.path}
              className={menu.path === routeActive ? "item" : "item"}
              
            >
              {menu.title}
            </a>
          </div>
        ))}
      </header>
      <div className="header-content">
        <div className="main-stickman">
          <img src={legendary} alt="Stickmaninvestor Legendary" />
        </div>
        <div className="social-networks">
          <div>
            <a href={util.discordUrl} target="_blank" rel="noreferrer">
              <img src={discord} alt="discord" />
            </a>
          </div>
          <div>
            <a href={util.twitterUrl} target="_blank" rel="noreferrer">
              <img src={twitter} alt="twitter" />
            </a>
          </div>
          <div>
            <a href={util.instagramUrl} target="_blank" rel="noreferrer">
              <img src={instagram} alt="instagram" />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Header;
