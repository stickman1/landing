import './App.css';
import Header from './header/Header';
import Welcome from './welcome/Welcome';
import RoadMap from './roadMap/RoadMap'
// import Faq from './faq/Faq'
import Team from './team/Team'
import React from 'react'
import Footer from './footer/Footer';

function App() {
  let [routeActive, setRouteActive] = React.useState('home');

  function goTo(path, e) {
    e && e.preventDefault();
    setRouteActive(path);
    window.scroll(0, document.getElementById(path).offsetTop - document.getElementById('header').clientHeight);
  }
  return (
    <main>
      <Header goTo={goTo} routeActive={routeActive}></Header>
      <div className='background-content'>
      <Welcome goTo={goTo}></Welcome>
      <RoadMap></RoadMap>
      <Team></Team>
      {/*<Faq></Faq>*/}
      </div>
      
      <Footer></Footer>
    </main>
  );
}

export default App;
