import stickman03 from './../assets/Stickmaninvestor-03.svg';
import stickman07 from './../assets/Stickmaninvestor-07.svg';
import logoHechado from './../assets/logo-hechado.svg';
import solana from './../assets/solana-logo-horizontal-gradient-dark.png';

import './Welcome.css'
import React from 'react'


function Welcome({ goTo }) {

    return (
        <section id="welcome" >
            <div className="background-stickman left" style={{ backgroundImage: "url(" + stickman07 + ")" }}></div>
            <div className="background-stickman right" style={{ backgroundImage: "url(" + stickman03 + ")" }}></div>
            <div className='content'>
                <div className='top'>

                    Welcome to <br />
                    <img src={logoHechado} alt="STICKMAN INVESTOR" />
                </div>
                <div className='main'>
                <p>
                    Stickman Investor is collection of 500 investment lovers, who have decided to raise their capital in order to carry out certain financial operations.
                </p><p>
                    This collection will allocate a part of the proceeds to the creation of an investment holding company in the metaverse,  also a land will be purchased and an operation office will be created.
                </p><p>
                    It will have a DAO system  in which NFT’s owner will have a vote.
                </p>
                <p>
                    Each NFT represents a % of the total holding money, therefore the NFTs will always be backed by a stronger value than by the mere fact of the value that can be given to a piece of art (something subjective).
                </p>
                <p>
                    Our project  will be the first holding company of the metaverse and among the owners of NFT it is expected to create a great fortune and cache within the metaverse.
                </p>
                <p>
                    Owning a stake in this holding will be something very exclusive due to its scarcity.
                </p>
                <p>The owners will be able to sell that NFT in the market or there will be  the possibility to cash that NFT for the corresponding money and will be burned or put up for sale at twice the price of what each NFT is worth.
                </p>
                <p>We want our investors to feel proud of being owners of one of our amazing NFT and with the holding company decisions can be made such as buying a land in the metaverse, creating  an NFT art project or distributing benefits among investors can be made.
                </p>
                <p>We have a whole world of possibilities with this holding company and we will exploit it to the fullest.
                </p>
                </div>
                <div className='botton'>
                    <img src={solana} alt="SOLANA" />
                </div>
            </div>


        </section>
    );
}

export default Welcome;
