import discord from "../assets/discord.svg";
import twitter from "../assets/twitter.svg";
import instagram from "../assets/instagram.svg";
import logoStrecho from "../assets/logo estrecho.svg";
import './Footer.css';
import * as util from '../util'
function Footer() {
  return (
    <footer>
      <div className="column">
        <div>
          <a href={util.twitterUrl} className="social" target="_blank" rel="noreferrer" >
            {" "}
            <img src={twitter} className='social-icon' alt="twitter" /> @StickmanIn500
          </a>
        </div>
        <div>
          <a href={util.instagramUrl} className="social" target="_blank" rel="noreferrer">
            {" "}
            <img src={instagram} className='social-icon' alt="twitter" />
            @stickmaninvestor
          </a>
        </div>
      </div>
      <div className="column center">
        <img src={logoStrecho} alt="STICKMAN INVESTOR" />
        <br></br>
        <span>COPYRIGTH 2021 - STICKMAN INVESTOR</span>
      </div>
      <div className="column">
        <div>
          <a href={util.discordUrl} className="social" target="_blank" rel="noreferrer">
            {" "}
            JOIN US ON DISCORD <img src={discord} className='social-icon' alt="discord" />
          </a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
