import './RoadMap.css'
import stage1 from '../assets/stage1.svg'
import stage2 from '../assets/stage2.svg'
import stage3 from '../assets/stage3.svg'
import stage4 from '../assets/stage4.svg'
import stage5 from '../assets/stage5.svg'

function RoadMap() {
    const phases = [{
        date: 'STAGE 1',
        content: <div>
            <ul>
                <li>Website development</li>
                <li>Discord server creation</li>
                <li>Social network design</li>
                <li>Mint configuration</li>
            </ul>
        </div>,
        image: stage1,
        isDone: true
    }, {
        date: 'STAGE 2',
        content: <div>
            <ul>
                <li>Attract people to the project trough marketing</li>
                <li>50 whitelist giveaway to 404 baepes holders</li>
                <li>Check our website server in order to know if it can support enough load</li>
                <li>Start selling NFT with a 1 minutes advance for whitelisted</li>
            </ul>
        </div>,
        image: stage2,
        isDone: false
    }, {
        date: 'STAGE 3',
        content: <div>
            <ul>
                <li>Distribution of the raising in the following way: 80% reinvested into the project and 20% for personal expenses.</li>
                <li>Bail out 80% of the investment into a Cake pool trough Cake staking</li>
                <li>Carry out first draw</li>
            </ul>
        </div>,
        image: stage3,
        isDone: false
    }, {
        date: 'STAGE 4',
        content: <div>
            <ul>
                <li>Make investment grow</li>
                <li>Sell more NFT</li>
                <li>Hire an artist to create a NFT personalized collection which will be given to every holder.</li>
            </ul>
        </div>,
        image: stage4,
        isDone: false
    }, {
        date: 'STAGE 5',
        content: <div>
            <ul>
                <li>Develop a new project</li>
                <li>Stickman’s holders’ advantages</li>
            </ul>
        </div>,
        image: stage5,
        isDone: false
    }
    ];
    return (
        <section id="roadmap" >
            <h3 className="title">Road Map</h3>
            <div className="phases">
                {phases.map((step, key) =>
                    <div key={key} className="step">
                        <div className="header">
                            <div className={"line " + 
                                (key === 0 ? 'line-first ' : '') + 
                                ((key === phases.length - 1) ? 'line-last' : '')}>
                                <div className={step.isDone ? 'circle active' : 'circle'}></div>
                            </div>
                        </div>
                        <div className={step.isDone ? 'content active' : 'content'}>
                            <img src={step.image} alt={step.date}></img>
                            <p className="step-date">{step.date}</p>
                            {step.content}
                        </div>
                    </div>
                )}
            </div>

        </section>
    );
}

export default RoadMap;
